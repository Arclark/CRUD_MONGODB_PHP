<?php 

require '../modelos/Materia.php';

$materia= new Materia();

$idmateria=isset($_POST['idmateria'])?trim($_POST['idmateria']):"";
$nombreMateria=isset($_POST['nombre'])?trim($_POST['nombre']):"";


switch ($_GET['operacion']) {
	case 'guardaryeditar':
		if (empty($idmateria)) {
			$respuesta=$materia->insertar($nombreMateria);
			echo $respuesta ? "Materia registrada":"Materia no se pudo resgistrar";
		}else{

			$respuesta=$materia->editar($idmateria,$nombreMateria);

			echo $respuesta ? "Materia actualizada":"Materia no se pudo actualizar";
		}
		break;

		case 'desactivar':
		
			$respuesta=$materia->desactivar($idmateria);
			echo $respuesta ? "Materia Desactivada":"Materia no se puede desactivar";	

		break;

		case 'activar':
		
			$respuesta=$materia->activar($idmateria);
			echo $respuesta ? "Materia Activada":"Materia no se puede activar";	

		break;	


		case 'mostrar':

			$respuesta=$materia->mostrar($idmateria);

			echo json_encode($respuesta);

		break;


		case 'listar':
			
			//array que declaramos
			$datos=array();
			$id=0;

			foreach ($materia->listar() as $mate) {
				
				$datos[]=array(
					
					"0"=>($mate['condicion'])?"<button class='btn btn-warning' onclick=mostrar('".$mate['_id']."')><i class='fa fa-pencil'></i></button>          "."<button class='btn btn-danger' onclick=desactivar('".$mate['_id']."')><i class='fa fa-close'></i></button>":"<button class='btn btn-warning' onclick=mostrar('".$mate['_id']."')><i class='fa fa-pencil'></i></button>          "."<button class='btn btn-primary' onclick=activar('".$mate['_id']."')><i class='fa fa-check'></i></button>",
 					"1"=>$mate['_id'],
 					"2"=>$mate['nombre'],
 					"3"=>$mate['condicion']?"<span class='label bg-green'>Activo</span>":
 				"<span class='label bg-red'>Inactivo</span>"

				);
			}

			$resultados = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($datos), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($datos), //enviamos el total registros a visualizar
 			"aaData"=>$datos);
 		echo json_encode($resultados);	

		break;

}


 ?>