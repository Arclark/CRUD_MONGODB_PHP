<?php 

require '../config/Conexion.php';

class Materia extends conexion
{



function __construct()
{
	parent::__construct();
}


public function insertar($nombre){

	$colecctionMateria=$this->conn->sistema->materia;

	$insertColection=$colecctionMateria->insertOne(['nombre'=>$nombre,'condicion'=>'1']);

return $insertColection->getInsertedCount();

}






public function editar($idmateria,$nombre){

	$colecctionMateria=$this->conn->sistema->materia;

	$updateColecction=$colecctionMateria->updateOne(['_id'=> new \MongoDB\BSON\ObjectID($idmateria)],['$set'=>['nombre'=>$nombre]]);

	return $updateColecction->getModifiedCount();

}


public function desactivar($idmateria)
{
	$colecctionMateria=$this->conn->sistema->materia;

	$desactivarMateria=$colecctionMateria->updateOne(['_id'=> new \MongoDB\BSON\ObjectID($idmateria)],['$set'=>['condicion'=>'0']]);

	return $desactivarMateria->getModifiedCount();

}

public function activar($idmateria)
{
		$colecctionMateria=$this->conn->sistema->materia;

		$activarMateria=$colecctionMateria->updateOne(['_id'=> new \MongoDB\BSON\ObjectID($idmateria)],['$set'=>['condicion'=>1]]);

	return $activarMateria->getModifiedCount();

}


public function mostrar($idmateria)
{ 
		$colecctionMateria=$this->conn->sistema->materia;

		$mostrarMateria=$colecctionMateria->findOne(['_id'=> new \MongoDB\BSON\ObjectID($idmateria)]);


		return  $mostrarMateria;
}

public function listar()
{
	
		$colecctionMateria=$this->conn->sistema->materia;

		$listarMateria=$colecctionMateria->find();

		return $listarMateria;

}


public function select()
{
		$colecctionMateria=$this->conn->sistema->materia;

		$activosMateria=$colecctionMateria->find(['condicion'=>1]);

		return $activosMateriaMateria;
}

}
//

	


?>