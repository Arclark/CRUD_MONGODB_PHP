<?php
require 'header.php';
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Materia <button class="btn btn-success" id="btnAgregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button></h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                 <div class="panel-body table-responsive" id="listadoregistros"> 
         
            <table class="table table-striped table-bordered table-condensed table-hover" id="listadoMateria">
              <thead>
                <tr>
                  <th>Opciones</th>
                  <th style="display: none">Código</th>
                  <th>Nombre</th>
                  <th>Condición</th>                                    
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>

            
        </div>
     
      
         <form name="formularioMateria" id="formularioMateria" method="POST" action="">
      
<div class="main-box-body clearfix" id="formularioregistros">
         
            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <label><span>Nombre</span></label>
              <input type="hidden" name="idmateria" id="idmateria">
              <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese nombre de la materia" required>
            </div>
            
          
            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
              <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>



            </div>

            </div>
          </form>
        
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
require 'footer.php';
?>
<script type="text/javascript" src="scripts/materia.js"></script>