var tabla;

//Función que se ejecuta al inicio
function init(){
	mostrarform(false);
	listar();

	$("#formularioMateria").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#idmateria").val("");
	$("#nombre").val("");
	
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnAgregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnAgregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#listadoMateria').dataTable(
	{	
		"bStateSave": true,
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],


		"ajax":
				{
					url: '../controladores/materia.php?operacion=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},

		 "columnDefs": [
            {

                "targets": [1], //ocultar de la vista las columnas
                "visible": false,
                "searchable": false
            },{
      "targets": [0,3], // your case first column
      "className": "text-center",
      
 				}

            ],		

		"bDestroy": true,
		"iDisplayLength": 6,//Paginación
	    "order": [[ 2, "asc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formularioMateria")[0]);

	$.ajax({
		url: "../controladores/materia.php?operacion=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {       


        
	    	swal({
				  title: datos,
				  text: "",
				  icon: "success",
				  button: "Aceptar",
				  timer: 2000
				});

	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idmateria)
{
	$.post("../controladores/materia.php?operacion=mostrar",{idmateria : idmateria}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		
		$("#nombre").val(data.nombre);

		$("#idmateria").val(data._id.$oid);
 

 	})




 	console.log("erro", idmateria)
}




//Función para desactivar registros
function desactivar(idmateria)

{
					
	swal({
						  title: "¿Está Seguro de desactivar la materia?",
						  text: "",
						  icon: "info",
						  buttons:["Cancelar","Aceptar"],
						  dangerMode: true,
						})
						.then((willDelete) => {
						  if (willDelete) {

						  	

						  $.post("../controladores/materia.php?operacion=desactivar",{idmateria : idmateria}, function(e){
        					
        					swal({
								  title: e,
								  text: "",
								  icon: "success",
								  button: "Aceptar",
								  timer: 2000
								});

        					
	           				 tabla.ajax.reload();

        	});

						  }

	
						   
						});

	
	


}

//Función para activar registros
function activar(idmateria)
{
 
	swal({
						  title: "¿Está Seguro de activar la materia?",
						  text: "",
						  icon: "warning",
						  buttons:["Cancelar","Aceptar"],
						  dangerMode: true,
						})
						.then((willDelete) => {
						  if (willDelete) {


						  		$.post("../controladores/materia.php?operacion=activar", {idmateria : idmateria}, function(e){
        					swal({
								  title: e,
								  text: "",
								  icon: "success",
								  button: "Aceptar",
								  timer: 2000
								});
	           				 tabla.ajax.reload();
        						}); 


						  }
						});
}


init();